package com.miniblog.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniblog.model.Role;
import com.miniblog.model.RoleName;
import com.miniblog.model.dto.UserDTO;
import com.miniblog.model.dto.UserInfoDTO;
import com.miniblog.model.dto.UserLoginDTO;
import com.miniblog.security.jwt.JwtProvider;
import com.miniblog.security.jwt.JwtResponse;
import com.miniblog.security.jwt.ResponseMessage;
import com.miniblog.service.RoleService;
import com.miniblog.service.UserService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value="/api/auth", produces=MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UserService userService;
	
	@Autowired
	RoleService roleService;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginDTO loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		
		UserInfoDTO userInfo = userService.findByEmail(userDetails.getUsername());
		
		return ResponseEntity.ok(new JwtResponse(jwt, userInfo));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserDTO signUpRequest, 
			@RequestParam(name ="role") String[] role) {
	
		if (userService.existsByEmail(signUpRequest)) {
			return new ResponseEntity<>(new ResponseMessage("Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}
		
		try { 
				List<String> listRoles =  Arrays.asList(role);
				Set<Role> roles = new HashSet<>();
	
				listRoles.forEach(roleUser -> {
					switch (roleUser) {
					case "admin":
						Role adminRole = roleService.findByName(RoleName.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
						roles.add(adminRole);
	
						break;
					case "pm":
						Role pmRole = roleService.findByName(RoleName.ROLE_PM)
								.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
						roles.add(pmRole);
	
						break;
					default:
						Role userRole = roleService.findByName(RoleName.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
						roles.add(userRole);
					}
				});
	
			 signUpRequest.setRoles(roles);
			
		      userService.registerUser(signUpRequest);
			  return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
			  
		} catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}