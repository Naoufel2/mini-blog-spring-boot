package com.miniblog.model.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.miniblog.model.Role;

public class UserDTO {
	
		@NotBlank
	    @Size(min = 3, max = 50)
	    private String firstName;

	    @NotBlank
	    @Size(min = 3, max = 50)
	    private String lastName;

	    @NotBlank
	    @Size(max = 50)
	    @Email
	    private String email;
	    
	    @NotBlank
	    @Size(min = 6, max = 40)
	    private String password;
	    
	    private Set<Role> roles;
	    
	    public UserDTO() {
	    	super();
	    }
	    
	    public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

		public Set<Role> getRoles() {
			return roles;
		}

		public void setRoles(Set<Role> roles) {
			this.roles = roles;
		}

}
