package com.miniblog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import com.miniblog.model.Role;
import com.miniblog.model.RoleName;
import com.miniblog.repository.RoleRepository;
import com.miniblog.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	RoleRepository roleRepository;

	@Override
	public Optional<Role> findByName(RoleName roleName) {
		return roleRepository.findByName(roleName);
	}

}
