package com.miniblog.service;

import java.util.Optional;

import com.miniblog.model.Role;
import com.miniblog.model.RoleName;

public interface RoleService {
	
	Optional<Role> findByName(RoleName roleName);

}
